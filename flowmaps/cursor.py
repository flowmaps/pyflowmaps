import itertools
import pandas as pd
from collections.abc import MutableMapping
from collections import defaultdict
from pprint import pformat


class Document(MutableMapping):

    def __init__(self, doc):
        self._doc = doc

    def __getitem__(self, key):
        return self._doc[self.__keytransform__(key)]

    def __setitem__(self, key, value):
        self._doc[self.__keytransform__(key)] = value

    def __delitem__(self, key):
        del self._doc[self.__keytransform__(key)]

    def __iter__(self):
        return iter(self._doc)

    def __len__(self):
        return len(self._doc)

    def __repr__(self):
        return f"{type(self).__name__}({pformat(self._doc)})"

    def __keytransform__(self, key):
        return key


class Cursor:
    """Iterator containing the documents returned by a query."""

    def __init__(self, cursor):
        self._cursor = cursor

    def __iter__(self):
        return self

    # def __next__(self):
    #     return Document(next(self._cursor))

    def __next__(self):
        return next(self._cursor)

    def next(self):
        """Get next document."""
        return self.__next__()

    def copy(self):
        """Returns a copy of the cursor."""
        self._cursor, copied_cursor = itertools.tee(self._cursor)
        return self._make_copy(copied_cursor)

    def _make_copy(self, iterator):
        return self.__class__(iterator)

    def to_df(self, **kwargs):
        """Convert the cursor into a pandas DataFrame."""
        return pd.DataFrame(self._cursor, **kwargs)

    def to_list(self):
        """Convert the cursor into a list of documents. WARN: this consumes the cursor."""
        return list(self._cursor)

    def ids(self, key='id'):
        """Get a list with the documents ids. WARN: this consumes the cursor."""
        return sorted([doc[key] for doc in self._cursor])

    def to_index(self, key='id', value=None):
        """
        Convert the cursor into a dictionary.
        If value is not specified, the returned dict values will be the full documents.
        WARN: this consumes the cursor.
        """
        if value is None:
            return {doc[key]: doc for doc in self._cursor}
        else:
            return {doc[key]: doc[value] for doc in self._cursor}

    def to_dict(self, key, value):
        """
        Convert the cursor into a dictionary {key: value}.
        WARN: this consumes the cursor.
        """
        return self.to_index(key, value)

    def islice(self, start, stop):
        """Returns a slice of the cursor."""
        iterator = itertools.islice(self._cursor, start, stop)
        return self._make_copy(iterator)

    def rename(self, old_key, new_key):
        """Rename a field on all the documents."""
        def _rename_key(doc):
            doc[new_key] = doc.pop(old_key)
            return doc
        iterator = map(_rename_key, self._cursor)
        return self._make_copy(iterator)

    def group_by(self, group_by_fields, accum_fields=[], count=True):
        """Group by the specified fields, and aggregate (sum) the accum_fields."""
        agg = defaultdict(lambda: defaultdict(float))
        for doc in self._cursor:
            combined_key = tuple(doc[k] for k in group_by_fields)
            if count:
                agg[combined_key]['count'] += 1         # keep trace of the number of original documents (useful for example to check that there is not a missing day, when aggregating by date)
            for field in accum_fields:
                agg[combined_key][field] += doc[field]
        # rebuild the cursor
        iterator = (
            {
                **dict(zip(group_by_fields, k)),    # include all the groupping fields
                **v                                 # include all the accum accumulated fields
            }
            for k, v in agg.items()
        )
        return self._make_copy(iterator)

    def count(self):
        """Returns the number of documents in the cursor. WARN: this consumes the cursor."""
        return sum(1 for _ in self._cursor)

    def filter(self, fn):
        """
        Filter the documents.

        Args:
            fn: a function that receives a single argument (a document), and returns True
                if the document should be accepted and False if the document should be rejected
        """
        iterator = (doc for doc in self._cursor if fn(doc))
        return self._make_copy(iterator)

    def apply(self, fn, dest_field):
        """
        Apply a function to all documents and store the result in the specified field.

        Args:
            fn: a function that receives a single argument (a document)
            dest_field: field to store the result of executing fn
        """
        iterator = ({**doc, dest_field: fn(doc)} for doc in self._cursor)
        return self._make_copy(iterator)

    def interpolate_layer(self, fm, source_layer, target_layer, interpolate_fields, loc='id', keep_fields=[], show_warnings=True, **kwargs):
        """
        Interpolate data from source_layer to target_layer.
        This assumes that the original documents contain data about
        locations on the source_layer, that should be "projected" to the target_layer.

        Args:
            fm: FlowMaps object
            source_layer: source layer. The original documents should contain data
                about locations on this layer.
            target_layer: target layer.
            interpolate_fields: numerical fields containing the data that will be interpolated.  
            loc: field used as id.
            keep_fields: fields that will be copied untouched to the resulting documents.
        """
        overlaps_dict = fm.overlaps(source_layer, target_layer, **kwargs).get().to_dict()
        return self.interpolate(overlaps_dict, loc=loc, interpolate_fields=interpolate_fields, keep_fields=keep_fields, show_warnings=show_warnings)

    def interpolate(self, overlaps_dict, interpolate_fields, loc='id', keep_fields=[], show_warnings=True):
        """
        Interpolate data using a overlaps dict.

        Args:
            overlaps_dict: dictionary containing the overlaps between the source and target layer.
                
                .. code-block:: text

                    overlaps_dict = {'source_id': [('target_id', ratio), ...], ...}
            
            interpolate_fields: numerical fields containing the data that will be interpolated.  
            loc: field used as id.
            keep_fields: fields that will be copied untouched to the resulting documents.
        """
        iterator = self._interpolate(self._cursor, overlaps_dict, loc, interpolate_fields, keep_fields, show_warnings)
        return self._make_copy(iterator)

    @staticmethod
    def _interpolate(cursor, overlaps_dict, loc, interpolate_fields, keep_fields, show_warnings):
        for doc in cursor:
            contrib = overlaps_dict.get(doc[loc], [])
            if not contrib and show_warnings:
                print(f"WARN: no overlaps for id: {doc[loc]}, skipping..")
                continue

            for area_id, ratio in contrib:
                yield {
                    'contrib': (doc[loc], ratio),
                    loc: area_id,
                    **{field: doc.get(field) for field in keep_fields},
                    **{field: ratio*doc[field] for field in interpolate_fields},
                }
