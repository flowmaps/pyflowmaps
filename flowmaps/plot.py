import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import random



def plot_layer(geojson, other_areas=[], df=None, loc='_id', 
               color_field='events', zoom=5, opacity=0.5, centroid=(),
               mapbox_style="carto-positron", title=None):
    if df is None:
        df = pd.DataFrame([{'_id': area['id'], color_field: random.randint(-5,5)} for area in geojson['features']])

    if len(centroid) == 2:
        (lon, lat) = centroid
    else:
        (lon, lat) = geojson['features'][0]['centroid']

    fig = px.choropleth_mapbox(data_frame=df,
                                geojson=geojson,
                                locations=loc,
                                color=color_field,
                                color_continuous_scale="Viridis",
                                hover_data=df.columns,
                                mapbox_style=mapbox_style,
                                opacity=opacity,
                                zoom=zoom,
                                center={"lat":lat, "lon":lon},
                                width=1080, height=820)

    return fig


# def plot_areas_geojson(areas_geojson, other_areas=[], zoom=5, df=None, loc='id', color='cases', title=None):
#     if df is None:
#         df = pd.DataFrame([{'id': area['id'], 'cases': random.random()} for area in areas_geojson['features']])
#     lon_lat = areas_geojson['features'][0]['geometry']['coordinates'][0][0]
#     if len(lon_lat) > 2:
#         lon, lat = lon_lat[0]
#     else:
#         lon, lat = lon_lat
#     fig = px.choropleth_mapbox(df, geojson=areas_geojson,
#                            locations=loc, color=color,
#                            title=title,
#                            color_continuous_scale="Inferno_r",
#                            #range_color=(0, 1),
#                            hover_data=df.columns,
#                            mapbox_style="carto-positron", opacity=0.5,
#                            zoom=zoom, center = {"lat": lat, "lon": lon})
#     mapbox_layers = [
#         { 'sourcetype': 'geojson', 'source': area_geojson,
#           'below': '', 'type': 'line', 
#           'color': '#ffd966', 'opacity': 1, 
#           'line': {'width': 2.5}
#         } for area_geojson in other_areas]
#     fig.layout.update(mapbox_layers=mapbox_layers)
#     fig.update_layout(margin={"r":0,"t":30,"l":0,"b":0})
#     return fig


def plot(polygons_geojson=None, points_geojson=None, border_areas=[], df=None, 
         cases_by_id=None, loc='id', color_field='events', default=None, title=None,
         zoom=5, opacity=0.5, colorscale='Viridis_r', color_range=None, mapbox_style='carto-positron',
         **kwargs):
    fig = go.Figure()
    (lon, lat) = (None, None)

    if polygons_geojson:
        (lon, lat) = polygons_geojson['features'][-1]['centroid']

        if df is not None:
            df_ids = df[loc].to_list()
            polygons_ids = [feat['id'] for feat in polygons_geojson['features']]
            if len(df_ids) != len(set(df_ids)):
                print('WARN: some ids in the df are repeated')

            if len(set(df_ids)) != len(set(polygons_ids)):
                print(f'WARN: number of distinct polygons and df rows do not match: {len(set(polygons_ids))} != {len(set(df_ids))}')

            if len(set(df_ids) - set(polygons_ids)) > 0:
                print(f'WARN: {len(set(df_ids) - set(polygons_ids))} ids in the df do not have a corresponding polygon')

            if len(set(polygons_ids) - set(df_ids)) > 0:
                print(f'WARN: {len(set(polygons_ids) - set(df_ids))} polygons do not have a corresponding row in the df')

        if df is None:
            if cases_by_id is not None:
                df = pd.DataFrame(cases_by_id.items(), columns=[loc, color_field])
            else:
                df = pd.DataFrame([(feat['id'], -1) for feat in polygons_geojson['features']], columns=[loc, color_field])

        if default is not None:
            # fill with default if no in in df
            df_ids = df[loc].to_list()
            geojson_ids = [feat['id'] for feat in polygons_geojson['features']]
            new_rows = []
            for area_id in geojson_ids:
                if area_id not in df_ids:
                    new_rows.append({loc: area_id, color_field: default})
            print(f"WARN: filling the df rows {[r[loc] for r in new_rows]} with default value {default}")
            df = pd.concat([df, pd.DataFrame(new_rows)])

        fig.add_trace(
            go.Choroplethmapbox(
                geojson=polygons_geojson,
                locations=df[loc],
                z=df[color_field],
                colorscale=colorscale,
                text=df[loc],
                hovertemplate = '<b>'+loc+'</b>: %{text}<br>'+
                                '<b>'+color_field+'</b>: %{z}<br>',
                zmin=color_range[0] if color_range else None,
                zmax=color_range[1] if color_range else None,
                # marker_line_width=1,
                marker_opacity = opacity,
                **kwargs
            )
        )

    if points_geojson:
        if lon is None or lat is None:
            (lon, lat) = points_geojson['features'][-1]['centroid']
        fig.add_trace(
            go.Scattermapbox(
                lon = [feat['geometry']['coordinates'][0] for feat in points_geojson['features']],
                lat = [feat['geometry']['coordinates'][1] for feat in points_geojson['features']],
                mode = 'markers',
                # marker=go.scattermapbox.Marker(size=10),
                hoverinfo = 'text',
                text = [feat['id'] for feat in points_geojson['features']],
            )
        )
    
    mapbox_layers = [
        { 'sourcetype': 'geojson', 'source': geojson_feat.to_geojson(),
          'below': '', 'type': 'line', 
          'color': '#ffd966', 'opacity': 1, 
          'line': {'width': 2.5}
        } for geojson_feat in border_areas
    ]
    
    fig.layout.update(mapbox_layers=mapbox_layers)

    fig.update_layout(mapbox_style=mapbox_style,
                      mapbox_zoom=zoom,
                      title=title,
                      mapbox_center = {"lat": lat, "lon": lon})
    fig.update_layout(margin={"r":0,"t":30,"l":0,"b":0})
    return fig


def plot_geojson(geojson, **kwargs):
    polygons = [feat for feat in geojson['features'] if feat['geometry']['type'] in ['Polygon', 'MultiPolygon']]
    if polygons:
        polygons_geojson = {
            'type': 'FeatureCollection',
            'features': polygons
        }
        kwargs['polygons_geojson'] = polygons_geojson

    points = [feat for feat in geojson['features'] if feat['geometry']['type'] == 'Point']
    if points:
        points_geojson = {
            'type': 'FeatureCollection',
            'features': points
        }
        kwargs['points_geojson'] = points_geojson

    return plot(**kwargs)


# def plot_mobility(mobility, geojson):
#     centroids_index = {feat['id']: feat['centroid'] for feat in geojson['features']}
#     mobility = mobility.enrich(centroids_index, key='origen', dest_field='origen_centroid')
#     mobility = mobility.enrich(centroids_index, key='destino', dest_field='destino_centroid')


# def plot_mobility(geojson, mobility_df, mode='outgoing', color_field='flujo', show_lines=False, zoom=5):
#     mode_to_loc = {
#         'incoming': 'origen',
#         'outgoing': 'destino',
#     }
#     loc = mode_to_loc.get(mode)

#     centroids_index = {feat['id']: feat['centroid'] for feat in geojson['features']}
#     (lon, lat) = geojson['features'][0]['centroid']
    
#     fig = px.choropleth_mapbox(data_frame=mobility_df,
#                                 geojson=geojson,
#                                 locations=loc,
#                                 color=color_field,
#                                 color_continuous_scale="Inferno_r",
#                                 # range_color=(0, 1),
#                                 hover_name=loc,
#                                 hover_data=['origen', 'destino', 'flujo'],
#                                 mapbox_style="carto-positron",
#                                 opacity=0.5,
#                                 zoom=zoom,
#                                 center={"lat":lat, "lon":lon})
#     if show_lines is True:
#         lines = []
#         for index, row in mobility_df.iterrows():
#             if row['origen'] in centroids_index and row['destino'] in centroids_index:
#                 lines.append([centroids_index[row['origen']], centroids_index[row['destino']]])

#         lines_geojson = {
#             'type': 'MultiLineString',
#             'coordinates': lines
#         }

#         mapbox_layers = [
#             { 'sourcetype': 'geojson', 'source': lines_geojson,
#               'below': '', 'type': 'line', 
#               'color': '#000000', 'opacity': 0.7, 
#               'line': {'width': 1}
#             }
#         ]

#         fig.layout.update(mapbox_layers=mapbox_layers)

#     return fig
