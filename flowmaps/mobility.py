import itertools
import numpy as np
from scipy.sparse import csr_matrix
from collections import defaultdict
from flowmaps.query import Query
from flowmaps.cursor import Cursor


class MobilityQuery(Query):
    """
    Class that represents a query to retrieve data from the mobility collections.
    """

    def __init__(self, db, collection_name, start_date=None, end_date=None, **kwargs):
        super().__init__(db, collection_name, **kwargs)

        if start_date is not None or end_date is not None:
            self.set_date_range(start_date, end_date)

    def get(self):
        cursor = self._execute_query()
        return MobilityCursor(cursor, self.collection_name)


class MobilityCursor(Cursor):
    """Iterator containing the documents returned by a MobilityQuery."""

    def __init__(self, cursor, collection_name):
        self.collection_name = collection_name
        super().__init__(cursor)
    
    def _make_copy(self, iterator):
        return self.__class__(iterator, self.collection_name)

    def to_dictdict(self, row='origen', col='destino', aggregate_field='viajes'):
        """
        Returns a dict of dicts containing the mobility matrix.
        
        Example:

        .. code-block:: text

            mobility = cursor.to_dictdict()
            mobility['id1']['id2']  # get the trips going from id1 to id2
        """
        matrix = defaultdict(lambda: defaultdict(float))
        for mov in self._cursor:
            matrix[mov[row]][mov[col]] += mov[aggregate_field]
        return dict(matrix)

    def to_csr_matrix(self, field, source_field='origen', target_field='destino', orig_map=None, dest_map=None, grow_orig_map=False, grow_dest_map=False):
        """
        Returns a scipy CSR matrix containing the mobility.
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.csr_matrix.html#scipy.sparse.csr_matrix

        The rows represent the source areas, and the columns represent the target areas.

        Returns: 3-tuple

            matrix: csr matrix containing the mobility
            
            row_map (dict): dict containing the map id-index for rows
            
            col_map (dict): dict containing the map id-index for columns
        
        Example:

        .. code-block:: text

            mobility, row_map, col_map = cursor.to_csr_matrix()
            mobility[i, j]  # get the trips going from i to j
        """

        row_map = orig_map or {}
        col_map = dest_map or {}
        data = []
        col = []
        row = []
        for doc in self._cursor:
            if orig_map is None or grow_orig_map:
                idx1 = row_map.setdefault(doc[source_field], len(row_map))
            else:
                idx1 = row_map[doc[source_field]]

            if dest_map is None or grow_dest_map:
                idx2 = col_map.setdefault(doc[target_field], len(col_map))
            else:
                idx2 = col_map[doc[target_field]]

            data.append(doc[field])
            row.append(idx1)
            col.append(idx2)
        return csr_matrix((data, (row, col))), row_map, col_map

    def to_matrix(self, field, ids, source_field='origen', target_field='destino'):
        """
        Returns a scipy CSR matrix containing the mobility.
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.csr_matrix.html#scipy.sparse.csr_matrix

        The rows represent the source areas, and the columns represent the target areas.

        Returns:

            matrix: csr matrix containing the mobility

        Example:

        .. code-block:: text

            M = cursor.to_matrix('viajes', mitma_ids)
            i = mitma_ids.index('01001_AM')
            j = mitma_ids.index('01031_AM')
            M[i, j]  # get the trips going from i to j
        """

        data = []
        col = []
        row = []
        ids_index = {id_: i for (i, id_) in enumerate(ids)}
        for doc in self._cursor:
            i = ids_index[doc[source_field]]
            j = ids_index[doc[target_field]]
            data.append(doc[field])
            row.append(i)
            col.append(j)
        return csr_matrix((data, (row, col)), shape=(len(ids), len(ids)))

    def interpolate_both_layers(self, overlaps, fields):
        """
        Interpolate origin and destination layers using the same overlaps dict.

        Args:
            overlaps: dictionary containing the overlaps between the source and target layer.

                .. code-block:: text

                    overlaps = {'source_id': [('target_id', ratio), ...], ...}

            fields: fields to be interpolated.
        """
        iterator = self._interpolate_both_layers(self._cursor, overlaps, fields)
        return self._make_copy(iterator)

    @staticmethod
    def _interpolate_both_layers(cursor, overlaps, fields):
        for doc in cursor:
            
            source_contrib = overlaps.get(doc['origen'], [])
            if not source_contrib:
                print(f"WARN: no overlaps for id: {doc['origen']}, skipping..")
                continue
            # assert sum(ratio for area_id, ratio in source_contrib) == 1, source_contrib
            total_ratio = sum(ratio for area_id, ratio in source_contrib)
            if total_ratio < 0.95 or total_ratio > 1.01:
                print(f"WARN: ratios do not sum up to 1: {source_contrib} for area_id (origen): {doc['origen']}. Total sum: {total_ratio}")

            target_contrib = overlaps.get(doc['destino'], [])
            if not target_contrib:
                print(f"WARN: no overlaps for id: {doc['destino']}, skipping..")
                continue
            # assert sum(ratio for area_id, ratio in target_contrib) == 1, target_contrib
            total_ratio = sum(ratio for area_id, ratio in target_contrib) 
            if total_ratio < 0.95 or total_ratio > 1.01:
                print(f"WARN: ratios do not sum up to 1: {target_contrib} for area_id (destino): {doc['destino']}. Total sum: {total_ratio}")

            for (source_id, source_ratio), (target_id, target_ratio) in itertools.product(source_contrib, target_contrib):
                yield {
                    # 'original': doc,
                    'contrib': ((doc['origen'], source_ratio), (doc['destino'], target_ratio)),
                    'origen': source_id,
                    'destino': target_id,
                    **{field: source_ratio*target_ratio*doc[field] for field in fields},
                }


    def interpolate_origin_layer(self, fm, source_layer, target_layer, fields, show_warnings=True, **kwargs):
        """
        Interpolate origins (project origins to a different layer).

        Args:
            fm: FlowMaps object
            source_layer: source layer. The original documents should contain data
                about locations on this layer.
            target_layer: target layer.
            fields: numerical fields containing the data that will be interpolated.  
        """
        return self.interpolate_layer(fm=fm,
                                     source_layer=source_layer, target_layer=target_layer,
                                     loc='origen', 
                                     interpolate_fields=fields, keep_fields=['destino', 'date', 'evstart', 'evend'],
                                     show_warnings=show_warnings, **kwargs)


    def interpolate_destination_layer(self, fm, source_layer, target_layer, fields, show_warnings=True, **kwargs):
        """
        Interpolate destinations (project destinations to a different layer).

        Args:
            fm: FlowMaps object
            source_layer: source layer. The original documents should contain data
                about locations on this layer.
            target_layer: target layer.
            fields: numerical fields containing the data that will be interpolated.  
        """
        return self.interpolate_layer(fm=fm,
                                     source_layer=source_layer, target_layer=target_layer,
                                     loc='destino', 
                                     interpolate_fields=fields, keep_fields=['origen', 'date', 'evstart', 'evend'],
                                     show_warnings=show_warnings, **kwargs)
