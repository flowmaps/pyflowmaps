import geojson
import visvalingamwyatt

from flowmaps.plot import plot_geojson


class FeatureCollection(geojson.FeatureCollection):

    def plot(self, **kwargs):
        """Plot geojson layer using plotly. Returns a plotly figure."""
        return plot_geojson(dict(self), **kwargs)

    def simplify_polygons(self, **kwargs):
        """
        Simplify polygons to reduce resolution (number of points).

        Args:
            ratio (float): fraction of points that will be kept.
                Example: 0.5 will discard half of the points
            min_points: minimum number of points to keep for each polygon

        Uses this: https://pypi.org/project/visvalingamwyatt/
        """
        self = simplify_geojson_geometry(self, **kwargs)
        return self

    def to_geojson(self):
        """Returns self. Just for compatibility."""
        return self

    def ids(self):
        """Returns the list of ids."""
        return [feat['id'] for feat in self['features']]

    def save(self, filename):
        """Save a geojson to a file."""
        with open(filename, 'w') as f:
           geojson.dump(self, f)

    @classmethod
    def load(cls, filename):
        """Load a geojson from a file."""
        with open(filename, 'r') as f:
            d = geojson.load(f)
            return cls(features=d['features'])


def load_geojson(filename):
    """Load a geojson from a file."""
    with open(filename) as f:
        geo = geojson.load(f)
        return FeatureCollection(geo['features'])


def area_to_geojson(area):
    return geojson.Feature(**area['feat'],
                           id=area.get('id'),
                           centroid=area.get('centroid'))


def layer_to_geojson(collection):
    features = [area_to_geojson(area)
                for area in collection]
    return FeatureCollection(features)


def simplify_area_geometry(area, ratio=0.3, min_points=100):
    area['feat'] = simplify_feat(area['feat'], ratio, min_points)
    return area


def simplify_layer_geometry(collection, ratio=0.3, min_points=100):
    return map(lambda x: simplify_area_geometry(x, ratio=ratio, min_points=min_points), collection)


def simplify_geojson_geometry(geojson, ratio=0.3, min_points=100):
    """
    Simplify polygons to reduce resolution (number of points).

    Args:
        ratio (float): fraction of points that will be kept.
            Example: 0.5 will discard half of the points
        min_points: minimum number of points to keep for each polygon

    Uses this: https://pypi.org/project/visvalingamwyatt/
    """
    if geojson.type == 'Feature':
        geojson = simplify_feat(geojson, ratio, min_points)
    elif geojson.type == 'FeatureCollection':
        geojson.features = [simplify_feat(feat, ratio, min_points) for feat in geojson.features]
    return geojson


def simplify_feat(feat, ratio, min_points):
    if feat['geometry']['type'] == 'Point':
        return feat
    if feat['geometry']['type'] in ['Polygon', 'MultiLineString']:
        n_points = max(len(line) for line in feat['geometry']['coordinates'])
    if feat['geometry']['type'] == 'MultiPolygon':
        n_points = max(len(line) for polyg in feat['geometry']['coordinates'] for line in polyg)

    number = max(min_points, n_points*ratio)
    feat['geometry'] = visvalingamwyatt.simplify_geometry(feat['geometry'], number=number)
    return feat
