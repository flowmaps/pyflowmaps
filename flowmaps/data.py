import numpy as np
from collections import defaultdict
from flowmaps.query import Query
from flowmaps.cursor import Cursor

class DataQuery(Query):
    """
    Class that represents a query to retrieve data from the 'layers.data' collection.
    """

    def __init__(self, db, collection_name='layers.data', start_date=None, end_date=None, **kwargs):
        super().__init__(db, collection_name, **kwargs)

        if start_date is not None or end_date is not None:
            self.set_date_range(start_date, end_date)

    def get(self):
        cursor = self._execute_query()
        return DataCursor(cursor)


class DataCursor(Cursor):
    """Iterator containing the documents returned by a DataQuery."""

    def __init__(self, cursor):
        super().__init__(cursor)

    def aggregate(self, key='id', value='cases'):
        """
        Group by id and sum value.

        Returns: dictionary {id: sum(values)}
        """
        agg = defaultdict(float)
        for doc in self._cursor:
            agg[doc[key]] += doc[value]
        return agg

    def to_vector(self, id_map, key='id', value='cases'):
        """
        Returns data as a numpy array.
        
        Args:
            id_map (dict): dictionary containing the mapping id-index.
            key: optional.
            value: optional.
        """
        vector = np.zeros(len(id_map))
        for doc in self._cursor:
            idx = id_map[doc[key]]
            vector[idx] += doc[value]
        return vector
