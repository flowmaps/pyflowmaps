from datetime import datetime
import networkx as nx
import pandas as pd
import pytz
import random
import json
from pymongo import MongoClient
from backports.datetime_fromisoformat import MonkeyPatch
MonkeyPatch.patch_fromisoformat()  # fix fromisoformat() not available in python 3.6


def init_mongodb(mongodb_params, timezone='Europe/Madrid', database_name='FlowMaps'):
    tz = pytz.timezone(timezone)
    # This is needed to honour authSource database
    database_name = mongodb_params.get('authSource',database_name)
    client = MongoClient(**mongodb_params)
    codec_options = client.codec_options.with_options(tzinfo=tz, tz_aware=True)
    db = client.get_database(database_name, codec_options=codec_options)
    return db


# def get_cte_date(year, month, day, hour=0, minute=0):
#     timezone='Europe/Madrid'
#     tz = pytz.timezone(timezone)
#     date = datetime(year, month, day, hour, minute)
#     return tz.localize(date)


def date2string(date, date_frmt="%Y%m%d"):
    return date.strftime(date_frmt)

    
def create_weighted_digraph(weighted_edges):
    G = nx.DiGraph()
    G.add_weighted_edges_from(weighted_edges)
    return G


def network2frame(G, round=True):
    zones_flux_balance = {n: {'in':0, 'out':0, 'balance': 0} for n in G.nodes() }
    for e in G.edges():
        u, v = e
        viajes = G.edges[e]['weight']
        
        zones_flux_balance[u]['out'] += viajes
        zones_flux_balance[u]['balance'] -= viajes
        
        zones_flux_balance[v]['in'] += viajes
        zones_flux_balance[v]['balance'] += viajes
        
    data = pd.DataFrame(zones_flux_balance.values(), index=zones_flux_balance.keys())
    data['loc'] = data.index
    if round:
        data = data.astype({"balance": int, "in": int, "out": int,})
    
    return data


class MyJSONDecoder(json.JSONDecoder):
    def __init__(self, *args, **kwargs):
        json.JSONDecoder.__init__(self, object_hook=self.object_hook, *args, **kwargs)

    def object_hook(self, obj):
        if '$datetime' in obj:
            return datetime.fromisoformat(obj['$datetime'])
        return obj


class MyJSONEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, datetime):
            return {'$datetime' : obj.isoformat()}
        else:
            return json.JSONEncoder.default(self, obj)


def dump_json(d, filename, **kwargs):
    with open(filename, 'w') as f:    
        json.dump(d, f, cls=MyJSONEncoder, **kwargs)


def load_json(filename, **kwargs):
    with open(filename) as f:    
        return json.load(f, cls=MyJSONDecoder, **kwargs)
