import pytz
from datetime import datetime, timedelta
from collections import defaultdict
from pprint import pformat

from flowmaps.cursor import Cursor


class Query:
    """
    Base class that represents a Mongodb aggregation pipeline query.
    Used to build the aggregation pipeline step by step.
    """

    def __init__(self, db, collection_name, pipeline=None, **kwargs):
        self.db = db
        self.collection_name = collection_name
        self.collection = db.get_collection(collection_name)
        self.pipeline = pipeline or []
        self.filter(**kwargs)

    def __repr__(self):
        return f"{type(self).__name__}(collection='{self.collection_name}', pipeline={pformat(self.pipeline)})"

    def copy(self):
        """Returns a copy of the query."""
        return Query(self.db, self.collection_name, self.pipeline.copy())

    def get(self):
        """Execute pipeline and return a cursor."""
        cursor = self.collection.aggregate(self.pipeline)
        return Cursor(cursor)

    def get_first(self):
        """Execute pipeline and return the first document."""
        cursor = self.get()
        return cursor.next()

    def _execute_query(self):
        return self.collection.aggregate(self.pipeline)

    def show(self):
        """Print pipeline definition."""
        print(pformat(self.pipeline))

    def search_regex(self, key, regex):
        """Add a $match step to the pipeline, to search using a regular expresion."""
        self.pipeline.append({
            '$match': {
                key: {'$regex': regex}
            }
        })
        return self

    def add_steps(self, steps):
        """Manually add steps to the pipeline."""
        self.pipeline += steps
        return self

    def sort(self, key, ascending=True):
        """Add a $sort step to the pipeline."""
        self.pipeline.append({
            '$sort': {
                key: 1 if ascending else -1
            }
        })
        return self

    def project(self, fields):
        """
        Add a $project step to the pipeline.
        https://docs.mongodb.com/manual/reference/operator/aggregation/project/
        """
        if type(fields) is list:
            fields = {k: 1 for k in fields}
        self.pipeline.append({
            '$project': {
                **fields,
                '_id': 0,
            }
        })
        return self

    def filter(self, filters_dict={}, **kwargs):
        """
        Add a $match step to the pipeline. 
        Filter the documents using any mongodb-like filter.
        """
        if not filters_dict and not kwargs:
            return self
        self.pipeline.append({
            '$match': {
                **filters_dict,
                **{k: v for k, v in kwargs.items() if v is not None}
            }
        })
        return self

    def set_date_range(self, start_date, end_date):
        """
        Add a $match step to the pipeline, filtering for a date range. 
        """
        if self.collection in ['layers.data.consolidated', 'mitma_mov.daily_mobility_matrix']:
            # dates should be strings

            if start_date and type(start_date) != str:
                start_date = start_date.strftime('%Y-%m-%d')

            if end_date and type(end_date) != str:
                end_date = start_date.strftime('%Y-%m-%d')

            field = 'date'

        else:
            # dates should be datetimes

            if start_date and type(start_date) == str:
                start_date = datetime.strptime(start_date, "%Y-%m-%d")

            if end_date and type(end_date) == str:
                end_date = datetime.strptime(end_date, "%Y-%m-%d") + timedelta(days=1)

            # the dates should be localized
            tz = pytz.timezone('Europe/Madrid')

            try:
                if start_date:
                    start_date = tz.localize(start_date)
            except ValueError:
                pass # already localized

            try:
                if end_date:
                    end_date = tz.localize(end_date)
            except ValueError:
                pass # already localized

            field = 'evstart'

        filters = {}
        if start_date is not None:
            filters['$gte'] = start_date
        if end_date is not None:
            filters['$lt'] = end_date
        self.pipeline.append({
            '$match': {
                'evstart': filters
            }
        })
        return self

    def add_fields(self, fields_dict):
        """
        Add an $addFields step to the pipeline. 
        """
        self.pipeline.append({
            '$addFields': {
                **fields_dict
            }
        })
        return self

    def add_date_field(self, date_source='evstart', timezone='Europe/Madrid'):
        """
        Add a date string field to all the documents. 
        """
        self.add_fields({
            'date': {
                '$dateToString': {
                    'format': '%Y-%m-%d', 
                    'date': '$'+date_source,
                    'timezone': timezone
                }
            },
        })
        return self

    def add_hour_field(self, date_source='evstart', timezone='Europe/Madrid'):
        """
        Add an hour string field to all the documents. 
        """
        self.add_fields({
            'hour': {
                '$dateToString': {
                    'format': '%H', 
                    'date': '$'+date_source,
                    'timezone': timezone
                }
            },
        })
        return self

    def flatten_fields(self, keys):
        """
        Convert nested fields into root level fields. 
        This can be used as a pre-step before a group by step, given that 
        MongoDB's group by do not work with nested fields.
        
        Args:
            keys: a dictionary containing the renamings (e.g. {'d.cases': 'cases'}), 
                or a list of fields (e.g. ['d.cases']). 
                If using a list of fields, all dots "." will be replaced with underscores "_")
        """
        if type(keys) is list:
            keys_path = {k.replace('.', '_'): k for k in keys}
        else:
            keys_path = {v: k for k,v in keys.items()}
        self.pipeline.append({
            '$addFields': {
                **{k: '$'+path for k, path in keys_path.items()}
            }
        })
        return self


    # def group_by(self, keys, accum_fields, fix_paths=False, additional_specs={}, count=True):
    #     # FIX: it is not possible to use $sum over a subdocument field (anything with '.')
    #     # The solution used here is to replace '.' by '_' 
    #     # For example, d.cases is aggregated into d_cases (a root level field)
    #     # If passing fix_paths the field is after fixed back to d.cases
    #     accum_key_path = {k.replace('.', '_'): k for k in accum_fields}

    #     if count:
    #         additional_specs['count'] = {'$sum': 1}

    #     self.pipeline += [
    #         {
    #             '$group': {
    #                 '_id': {
    #                     key: '$'+key for key in keys
    #                 },
    #                 **{k: {'$sum': '$'+path} for k, path in accum_key_path.items()},
    #                 **additional_specs
    #             },
    #         }
    #     ]
    #     if fix_paths:
    #         self.pipeline += [  # fix paths back, example d_cases -> d.cases 
    #             {
    #                 '$set': {
    #                     **{path: '$'+k for k, path in accum_key_path.items()}
    #                 } 
    #             },
    #             {
    #                 '$project': {
    #                     **{k: 0 for k, path in accum_key_path.items()}
    #                 }
    #             }
    #         ]
    #     self.pipeline += [  # extract groupping keys from _id
    #         {
    #             '$addFields': {
    #                 **{key: '$_id.'+key for key in keys}
    #             }
    #         },
    #         {
    #             '$project': {
    #                 '_id': 0
    #             }
    #         }
    #     ]
    #     return self


    def group_by(self, keys, accum_fields, first_fields=[], additional_specs={}, count=True):
        """
        Group the documents by the specified keys and aggregate the accum_fields.
        Note: the fields must be root level fields.
        """

        if count:
            additional_specs['count'] = {'$sum': 1}

        self.pipeline += [
            {
                '$group': {
                    '_id': {
                        key: '$'+key for key in keys
                    },
                    **{k: {'$sum': '$'+k} for k in accum_fields},
                    **{k: {'$first': '$'+k} for k in first_fields},
                    **additional_specs
                },
            },
            {
                '$addFields': {  # extract groupping keys from _id
                    **{key: '$_id.'+key for key in keys}
                }
            },
            {
                '$project': {
                    '_id': 0
                }
            }
        ]
        return self


    def count_values(self, field):
        """Count number of appearances of each value of field"""
        self.pipeline += [
            {
                '$group': {
                    '_id': '$'+field,
                    'count': {'$sum': 1},
                    field: {'$first': '$'+field},
                }
            },
            {
                '$sort': {
                    'count': -1
                }
            }
        ]
        return self


    def ids(self, key='id'):
        """Execute the query and return the ids of the matching documents"""
        return self.project({'id': 1}).get().ids(key=key)

    def to_df(self):
        return self.get().to_df()
