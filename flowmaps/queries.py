import pandas as pd
import pytz
import itertools
from datetime import datetime, timedelta

def get_ev_data(fm, ev_id, ev_field_source, ev_field_dest, start_date, end_date, ids=None):
    query = fm.data(start_date=start_date, end_date=end_date, ev=ev_id)
    if ids is not None:
        query = query.filter({'id': {'$in': ids}})
    query = query.flatten_fields({ev_field_source: ev_field_dest})
    df = query.get().to_df()
    df = df[['id', 'evstart', ev_field_dest]]
    df = df.rename({'evstart': 'date'}, axis=1)
    return df


def get_data(fm, start_date, end_date, ev, field, ids=None, aggregate='total'):
    query = fm.data(start_date=start_date, end_date=end_date, ev=ev)

    # filter ids
    if ids is not None:
        query = query.filter({'id': {'$in': ids}})

    query = query.flatten_fields({field: 'cases'})

    # aggregate
    if aggregate == 'total':
        query = query.group_by(['id'], ['cases'])
    elif aggregate == 'daily':
        query = query.add_date_field()
        query = query.group_by(['id', 'date'], ['cases'])
    
    return query.get().to_df()


def get_population_by_layer(fm, layer, start_date=None, end_date=None, mitma_ids=None, ids=None):
    
    if start_date is None or end_date is None:
        start_date = datetime(2020, 3, 1, 0, 0) # default date range just before confinement
        end_date = datetime(2020, 3, 10, 0, 0)

    query = fm.query('mitma_mov.zone_movements').set_date_range(start_date, end_date)
    query = query.add_date_field()

    if mitma_ids is not None:
        query = query.filter({'id': {'$in': mitma_ids}})

    query = query.group_by(['date', 'id'], ['personas']) # aggregaty by day, id

    pop_cursor = query.get()
    pop_cursor = pop_cursor.group_by(['id'], ['personas'], count=True) # aggregate by id, adding all days
    pop_cursor = pop_cursor.apply(lambda x: x['personas']/max(x['count'], 1), 'population') # mean population in the date range
    pop_cursor = pop_cursor.interpolate_layer(fm,
                                    source_layer='mitma_mov',
                                    target_layer=layer,
                                    loc='id',
                                    interpolate_fields=['population'],
                                    keep_fields=[],
                                    show_warnings=False)
    if ids:
        pop_cursor = pop_cursor.filter(lambda x: x['id'] in ids)

    pop_cursor = pop_cursor.group_by(['id'], ['population'], count=False)
    pop_df = pop_cursor.to_df()
    return pop_df


def get_layer_to_layer_mobility(fm, source_layer, target_layer, start_date, end_date, target_ids=None, source_ids=None, aggregate='total'):
    query = fm.mobility('mitma_mov.daily_mobility_matrix')

    if target_ids:
        if target_layer == 'mitma_mov':
            target_mob_ids = target_ids
        else:
            target_overlaps = fm.overlaps(target_layer, 'mitma_mov').get().to_dict()
            target_mob_ids = [mob_area_id for id_ in target_ids for (mob_area_id, ratio) in target_overlaps[id_]]
        query = query.filter({'destino': {'$in': target_mob_ids}})

    if source_ids:
        if source_layer == 'mitma_mov':
            source_mob_ids = source_ids
        else:
            source_overlaps = fm.overlaps(source_layer, 'mitma_mov').get().to_dict()
            source_mob_ids = [mob_area_id for id_ in source_ids for (mob_area_id, ratio) in source_overlaps[id_]]
        query = query.filter({'origen': {'$in': source_mob_ids}})

    query.filter(date={'$gte': start_date.strftime('%Y-%m-%d'), '$lt': end_date.strftime('%Y-%m-%d')})
    mob_cursor = query.get()

    # interpolate origin to global layer (e.g. provinces)
    if source_layer != 'mitma_mov':
        mob_cursor = mob_cursor.interpolate_origin_layer(fm, source_layer='mitma_mov',
                                                target_layer=source_layer,
                                                fields=['viajes'],
                                                show_warnings=False)

    # interpolate destination to target layer (e.g. codigos_postales)
    if target_layer != 'mitma_mov':
        mob_cursor = mob_cursor.interpolate_destination_layer(fm, source_layer='mitma_mov',
                                                target_layer=target_layer,
                                                fields=['viajes'],
                                                show_warnings=False)

    # discard residues
    if target_ids:
        mob_cursor = mob_cursor.filter(lambda x: x['destino'] in target_ids)

    if source_ids:
        mob_cursor = mob_cursor.filter(lambda x: x['origen'] in source_ids)

    # aggregate
    if aggregate == 'total':
        mob_cursor = mob_cursor.group_by(['origen', 'destino'], ['viajes'])
    elif aggregate == 'daily':
        mob_cursor = mob_cursor.group_by(['origen', 'destino', 'date'], ['viajes'])

    return mob_cursor.to_df()


def get_risk_for_hotspot(fm, hotspot_id, target_layer, source_layer, source_ev_id, source_ev_field, start_date, end_date, source_ids=None):
    # TODO: use active_cases instead of daily cases
    mobility = get_layer_to_layer_mobility(fm, 
                                    source_layer=source_layer,
                                    target_layer=target_layer,
                                    start_date=start_date,
                                    end_date=end_date, 
                                    target_ids=[hotspot_id],
                                    source_ids=source_ids)

    population = get_population_by_layer(fm,
                                    layer=source_layer,
                                    ids=source_ids)

    data = get_data(fm, 
                    start_date=start_date,
                    end_date=end_date,
                    ev=source_ev_id,
                    field=source_ev_field,
                    ids=source_ids,
                    aggregate='total')

    df = pd.merge(mobility, population, how='outer', left_on='origen', right_on='id')
    df = pd.merge(df, data, how='outer', left_on='id', right_on='id')
    df = df.rename(columns={'count': 'num_days', 'personas': 'population'})
    df['cases'].fillna(1e-6, inplace=True)
    df = df[df['population'] != 0] # avoid division by zero
    df['risk'] = df.apply(lambda x: x['viajes']*x['cases']/x['population'], axis=1)
    df = df.sort_values('risk', ascending=False)
    return df


def get_layer_to_layer_risk(fm, source_layer, target_layer, source_ev_id, source_ev_field, start_date, end_date, source_ids=None, target_ids=None):
    # TODO: use active_cases instead of daily cases
    mobility = get_layer_to_layer_mobility(fm,
                                    source_layer=source_layer,
                                    target_layer=target_layer,
                                    start_date=start_date,
                                    end_date=end_date, 
                                    target_ids=target_ids,
                                    source_ids=source_ids,
                                    aggregate='total')
    if mobility.empty:
        raise Exception(f'There is no mobility data for the selected dates: {start_date} - {end_date}')

    population = get_population_by_layer(fm,
                                    layer=source_layer,
                                    ids=source_ids)

    data = get_data(fm, 
                    start_date=start_date,
                    end_date=end_date,
                    ev=source_ev_id,
                    field=source_ev_field,
                    ids=source_ids,
                    aggregate='total')

    df = pd.merge(mobility, population, how='outer', left_on='origen', right_on='id')
    df = pd.merge(df, data, how='outer', left_on='id', right_on='id')
    df = df.rename(columns={'count': 'num_days', 'personas': 'population', 'cases': 'cases_in_origin'})
    df['cases_in_origin'].fillna(1e-6, inplace=True)
    df = df[df['population'] != 0] # avoid division by zero
    df['risk'] = df.apply(lambda x: x['viajes']*x['cases_in_origin']/x['population'], axis=1)
    df = df.sort_values('risk', ascending=False)
    return df


def get_daily_mobility(fm, source_layer, target_layer, date_str):
    mob_cursor = fm.mobility('mitma_mov.daily_mobility_matrix', date=date_str).get()
    mob_cursor = mob_cursor.interpolate_origin_layer(fm, 
                                                    source_layer='mitma_mov',
                                                    target_layer=source_layer,
                                                    fields=['viajes'],
                                                    show_warnings=False)
    mob_cursor = mob_cursor.interpolate_destination_layer(fm, 
                                                         source_layer='mitma_mov',
                                                         target_layer=target_layer,
                                                         fields=['viajes'],
                                                         show_warnings=False)
    mob_cursor = mob_cursor.group_by(['origen', 'destino'], ['viajes'])
    return mob_cursor.to_df()


class MissingDataException(Exception):
    pass


def get_daily_risk(fm, source_layer, target_layer, source_ev_id, source_ev_field, date_str, window=7, timezone='Europe/Madrid', debug=False):
    tz = pytz.timezone(timezone)
    end_date = tz.localize(datetime.strptime(date_str, '%Y-%m-%d'))
    start_date = end_date - timedelta(days=window)

    source_ids = [d['id'] for d in fm.layer(source_layer).project(['id']).get().to_list()]
    target_ids = [d['id'] for d in fm.layer(target_layer).project(['id']).get().to_list()]

    # get active_cases estimation
    data = get_data(fm, start_date, end_date, source_ev_id, source_ev_field, aggregate='total')
    if debug:
        print(f'data df shape: {data.shape}')
    if data.empty:
        raise MissingDataException(f'There is no covid data for the selected range: {start_date} - {end_date}')
    data_dict = data.set_index('id')['cases'].to_dict()

    # get population
    population = get_population_by_layer(fm, source_layer, start_date=start_date, end_date=end_date)
    if population.empty:
        raise MissingDataException(f'There is no population data for the selected range: {start_date} - {end_date}')
    if debug:
        print(f'population df shape: {population.shape}')
    population_dict = population.set_index('id')['population'].to_dict()

    # get mobility data
    mobility = get_daily_mobility(fm, source_layer, target_layer, date_str)
    if debug:
        print(f'mobility df shape: {mobility.shape}')
    if mobility.empty:
        raise MissingDataException(f'There is no mobility data for the selected date: {date_str}')
    mobility_dict = mobility.set_index(['origen', 'destino'])['viajes'].to_dict()

    # first, build complete dataframe with all source-destino combinations
    df = pd.DataFrame([{'source': o, 'target': d} for o, d in itertools.product(source_ids, target_ids)])

    # enrich with population in source, trips and number of active_cases in source
    df['population'] = df['source'].apply(lambda x: population_dict.get(x))
    df['trips'] = df[['source', 'target']].apply(lambda r: mobility_dict.get((r['source'], r['target']), 0), axis=1)
    df['active_cases'] = df['source'].apply(lambda x: data_dict.get(x, 0))

    # calculate risk
    df['risk'] = df['trips']*df['active_cases']/df['population']
    df = df.sort_values('risk', ascending=False)
    return df
