

# id of the high resolution sublayer for each ccaa (when available)
ccaa_to_abs_layer = {
    '09': 'abs_09',
    '16': 'oe_16',
    '07': 'zbs_07',
    '15': 'zbs_15',
    '13': 'zon_bas_13',
}


# field that contains the name of each polygon (subfield of 'properties')
layers_name_fields = {
    'zbs_07': 'd_zbs',
    'abs_09': 'NOMSS', # NOMABS
    'zon_bas_13': 'DESBDT',
    'zbs_15': 'ZONA',
    'oe_16': 'ZONA_Nom1',
    'cnig_provincias': 'rotulo',
    'cnig_ccaa': 'rotulo',
}


# For each ccaa, the preferred sublayer where we have high resolution covid data
# Used by the dashboard
ccaa_data_sublayer = {
    '07': 'zbs_07',
    '09': 'abs_09',
    '13': 'zon_bas_13',
    '15': 'zbs_15',
    '16': 'oe_16',
}

# For each ccaa, the preferred ev where we have high resolution covid data
# Used by the dashboard
layer_to_ev = {
    'cnig_provincias': 'ES.covid_cpro',
    'cnig_ccaa': 'ES.covid_cca',
    'zbs_07': '07.covid_abs',
    'abs_09': '09.covid_abs',
    'zon_bas_13': '13.covid_abs',
    'zbs_15': '15.covid_abs',
    'oe_16': '16.covid_abs',
}


# For each ccaa, sublayers for which we have high resolution covid data,
# and the subfields of 'd' that contain the data.
# This includes: new_cases, total_cases and active_cases.
# Manually curated. Keys are ccaa ids
ccaa_data_sources = {
    '01': {}, # Andalucia
    '02': {}, # Aragon
    '03': {}, # Asturias
    '04': {}, # Baleares
    '05': {}, # Canarias
    '06': {   # Cantabria
        'incidence': [],
        'prevalence': [
            {
                'layer': 'cnig_municipios',
                'ev': '06.covid_cumun',
                'field': 'total_active'
            }
        ],
        'total': [
            {
                'layer': 'cnig_municipios',
                'ev': '06.covid_cumun',
                'field': 'total_cases'
            }
        ],
    },
    '07': {   # Castilla Leon
        'incidence': [
            {
                'layer': 'zbs_07',
                'ev': '07.covid_abs',
                'field': 'cases'
            }
        ],
        'prevalence': [],
        'total': [
            {
                'layer': 'zbs_07',
                'ev': '07.covid_abs',
                'field': 'total_cases'
            }
        ],
    },
    '08': {}, # Castilla la Mancha
    '09': {   # Cataluña
        'incidence': [
            {
                'layer': 'abs_09',
                'ev': '09.covid_abs',
                'field': 'cases'
            }
        ],
        'prevalence': [],
        'total': [],
    },
    '10': {}, # Valencia
    '11': {}, # Extremadura
    '12': {}, # Galicia
    '13': {   # Madrid
        'incidence': [
            {
                'layer': 'zon_bas_13',
                'ev': '13.covid_abs',
                'field': 'cases'
            }
        ],
        'prevalence': [],
        'total': [
            {
                'layer': 'zon_bas_13',
                'ev': '13.covid_abs',
                'field': 'total_cases'
            }
        ],
    },
    '14': {}, # Murcia
    '15': {   # Navarra
        'incidence': [
            {
                'layer': 'zbs_15',
                'ev': '15.covid_abs',
                'field': 'cases'
            }
        ],
        'prevalence': [],
        'total': [
            {
                'layer': 'zbs_15',
                'ev': '15.covid_abs',
                'field': 'total_cases'
            }
        ],
    },
    '16': {   # P.Vasco
        'incidence': [
            {
                'layer': 'oe_16',
                'ev': '16.covid_abs',
                'field': 'cases'
            }
        ],
        'prevalence': [],
        'total': [
            {
                'layer': 'oe_16',
                'ev': '16.covid_abs',
                'field': 'total_cases'
            }
        ],
    },
    '17': {}, # La Rioja
    '18': {}, # Ceuta
    '19': {}, # Melilla
}


# list of province ids that correspond to each ccaa. Keys are ccaa ids
ccaa_to_provinces = {
    '05': ['38', '35'],
    '06': ['39'],
    '10': ['03', '12', '46'],
    '11': ['06', '10'],
    '18': ['51'],
    '08': ['02', '19', '13', '16', '45'],
    '14': ['30'],
    '12': ['15', '36', '27', '32'],
    '19': ['52'],
    '03': ['33'],
    '17': ['26'],
    '07': ['09', '05', '34', '24', '42', '40', '37', '49', '47'],
    '04': ['07'],
    '16': ['48', '01', '20'],
    '02': ['44', '22', '50'],
    '09': ['17', '08', '25', '43'],
    '13': ['28'],
    '15': ['31'],
    '01': ['21', '11', '14', '18', '04', '29', '23', '41']
}


# names of each province
province_id_to_name = {
    '48': 'Bizkaia',
    '13': 'Ciudad Real',
    '26': 'La Rioja',
    '22': 'Huesca',
    '20': 'Gipuzkoa',
    '46': 'València/Valencia',
    '15': 'A Coruña',
    '18': 'Granada',
    '31': 'Navarra',
    '33': 'Asturias',
    '34': 'Palencia',
    '51': 'Ceuta',
    '29': 'Málaga',
    '06': 'Badajoz',
    '07': 'Illes Balears',
    '49': 'Zamora',
    '45': 'Toledo',
    '16': 'Cuenca',
    '17': 'Girona',
    '10': 'Cáceres',
    '11': 'Cádiz',
    '09': 'Burgos',
    '42': 'Soria',
    '43': 'Tarragona',
    '21': 'Huelva',
    '28': 'Madrid',
    '08': 'Barcelona',
    '52': 'Melilla',
    '03': 'Alacant/Alicante',
    '12': 'Castelló/Castellón',
    '23': 'Jaén',
    '27': 'Lugo',
    '05': 'Ávila',
    '47': 'Valladolid',
    '04': 'Almería',
    '25': 'Lleida',
    '02': 'Albacete',
    '32': 'Ourense',
    '36': 'Pontevedra',
    '39': 'Cantabria',
    '44': 'Teruel',
    '50': 'Zaragoza',
    '35': 'Las Palmas',
    '19': 'Guadalajara',
    '40': 'Segovia',
    '24': 'León',
    '14': 'Córdoba',
    '01': 'Araba/Álava',
    '30': 'Murcia',
    '37': 'Salamanca',
    '38': 'Santa Cruz de Tenerife',
    '41': 'Sevilla'
}


# corresponding ccaa id for each province id. Keys are province ids
province_to_ccaa = {
    '38': '05',
    '35': '05',
    '39': '06',
    '03': '10',
    '12': '10',
    '46': '10',
    '06': '11',
    '10': '11',
    '51': '18',
    '02': '08',
    '19': '08',
    '13': '08',
    '16': '08',
    '45': '08',
    '30': '14',
    '15': '12',
    '36': '12',
    '27': '12',
    '32': '12',
    '52': '19',
    '33': '03',
    '26': '17',
    '09': '07',
    '05': '07',
    '34': '07',
    '24': '07',
    '42': '07',
    '40': '07',
    '37': '07',
    '49': '07',
    '47': '07',
    '07': '04',
    '48': '16',
    '01': '16',
    '20': '16',
    '44': '02',
    '22': '02',
    '50': '02',
    '17': '09',
    '08': '09',
    '25': '09',
    '43': '09',
    '28': '13',
    '31': '15',
    '21': '01',
    '11': '01',
    '14': '01',
    '18': '01',
    '04': '01',
    '29': '01',
    '23': '01',
    '41': '01'
}

