from flowmaps.data import DataQuery
from flowmaps.layers import LayerQuery
from flowmaps.mobility import MobilityQuery
from flowmaps.overlaps import OverlapsQuery
from flowmaps.query import Query
from flowmaps.utils import init_mongodb


class FlowMaps:
    """
    Main FlowMaps database descriptor. It contains methods for accessing all the resources.

    Args:
        mongodb_params (dict): Example:

            .. code-block:: text

                mongodb_params = {
                    "host": ['****'],
                    "username":'****',
                    "password": '****',
                    "authSource": '****'
                }

        timezone: optional, default='Europe/Madrid'
    """

    def __init__(self, mongodb_params, timezone='Europe/Madrid'):
        self.db = init_mongodb(mongodb_params, timezone)

    def layer(self, layer_id, **kwargs):
        """Returns a LayerQuery to retrieve data from the 'layers' collection."""
        return LayerQuery(self.db, layer_id, **kwargs)

    def data(self, collection='layers.data', start_date=None, end_date=None, **kwargs):
        """Returns a DataQuery to retrieve data from the 'layers.data' collection."""
        return DataQuery(self.db, collection_name=collection, start_date=start_date, end_date=end_date, **kwargs)

    def mobility(self, collection, start_date=None, end_date=None, **kwargs):
        """Returns a MobilityQuery to retrieve data from the collection_name collection."""
        return MobilityQuery(self.db, collection, start_date, end_date, **kwargs)

    def query(self, collection, **kwargs):
        """Returns a Query object to retrieve data from the collection."""
        return Query(self.db, collection, **kwargs)

    def overlaps(self, layer1, layer2, **kwargs):
        """
        Returns a OverlapsQuery configured to query the 'layers.overlaps' collection.
        
        NOTE: By convention, the layer with less polygons is suposed to be on l, and the
        layer with more polygons is suposed to be on m. This method takes that into account automatically. 

        NOTE2: This method will check whether the overlaps are already precalculated
        and stored in 'layers.overlaps'. In case they are not, it will raise an exception
        and show the command that needs to be executed to compute the overlaps  
        """
        return OverlapsQuery(self.db, layer1, layer2, **kwargs)
