import json
from flowmaps.query import Query
from flowmaps.cursor import Cursor, Document
import flowmaps.geojson as geojson


class LayerQuery(Query):
    """
    Class that represents a query to retrieve data from the 'layers' collection.
    """

    def __init__(self, db, layer_id, **kwargs):
        self.db = db
        super().__init__(db, 'layers', layer=layer_id, **kwargs)

    def get(self):
        cursor = self._execute_query()
        return LayerCursor(cursor, self.db)


class LayerCursor(Cursor):
    """Iterator containing the documents returned by a LayerQuery."""

    def __init__(self, cursor, db):
        self.db = db
        super().__init__(cursor)

    def __next__(self):
        return PolygonDocument(next(self._cursor), self.db)

    def _make_copy(self, iterator):
        return self.__class__(iterator, self.db)

    def to_geojson(self):
        """Convert cursor to a geojson object."""
        return geojson.layer_to_geojson(self._cursor)

    def simplify_polygons(self, **kwargs):
        """Simplify polygons resolution."""
        iterator = geojson.simplify_layer_geometry(self._cursor, **kwargs)
        return self._make_copy(iterator)

    def plot(self, **kwargs):
        """Plot polygons using plotly. Returns a plotly figure."""
        fig = self.to_geojson().plot(**kwargs)
        return fig


class PolygonDocument(Document):
    """Document that represents a geographic polygon."""

    def __init__(self, doc, db):
        self.db = db
        super().__init__(doc)

    def to_geojson(self):
        """Convert to geojson."""
        return geojson.layer_to_geojson([self._doc])

    def simplify_geometry(self, **kwargs):
        """Simplify polygon resolution."""
        self._doc = geojson.simplify_area_geometry(self._doc)
        return self

    def plot(self, **kwargs):
        """Plot polygon using plotly. Returns a plotly figure."""
        return self.to_geojson().plot(**kwargs)

    def query_overlapping_polygons(self, target_layer, use_centroid=True):
        """
        Creates a query to find the polygons of the target layer that
        overlap with this polygon.

        Returns: a LayerQuery object
        """
        query = LayerQuery(self.db, target_layer)
        geometry = self['feat']['geometry']
        if use_centroid:
            query.filter({
                'centroid':  { 
                    '$geoWithin': {
                        '$geometry': geometry
                    }
                }
            })
        else:
            query.filter({
                'feat.geometry':  { 
                    '$geoIntersects': {
                        '$geometry': geometry
                    }
                },
            })
        return query

    @classmethod
    def from_file(cls, filename, db):
        """Load a polygon from a json file."""
        with open(filename) as f:
            doc = json.load(f)
        return cls(doc, db)

    def save(self, filename):
        """Save a polygon to a json file."""
        with open(filename, 'w') as f:
            json.dump(dict(self), f)
