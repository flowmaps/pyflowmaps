import numpy as np
from scipy.sparse import csr_matrix
from collections import defaultdict
from flowmaps.query import Query
from flowmaps.cursor import Cursor
from flowmaps.layers import LayerQuery


class OverlapsQuery(Query):
    """
    Class that represents a query to retrieve data from the 'layers.overlaps' collection.
    """

    def __init__(self, db, layer1, layer2, layer1_min_ratio=0, layer2_min_ratio=0, collection='layers.overlaps', **kwargs):
    # def __init__(self, db, layer1, layer2, layer1_min_ratio=1e-6, layer2_min_ratio=1e-6, **kwargs):
        self.layer1 = layer1
        self.layer2 = layer2
        super().__init__(db, collection, **kwargs)

        """
        IMPORTANT NOTE: 
        By convention, the layer with less polygons is suposed to be on l, and the
        layer with more polygons is suposed to be on m.
        This means that if we want to know which layer use as l and which one use 
        as m (to build the query) we need to perform a previous query, to count the
        number of polygons in each layer.
        Actually it is easier to just check/count which entries are present in the
        overlaps collection, as I am doing here.
        The perfect solution would be the two layers l and m were sorted in lexicographic
        order (we would not need to perform any preparative query)
        """
        pipeline = [
            {
                '$group': {
                    '_id': {
                        'layer1': '$l.layer', 
                        'layer2': '$m.layer'
                    }, 
                    'count': {
                        '$sum': 1
                    }
                }
            }
        ]
        cursor = self.collection.aggregate(pipeline)
        available_overlaps = {(doc['_id']['layer1'], doc['_id']['layer2']): doc['count'] for doc in cursor}

        count1 = available_overlaps.get((layer1, layer2), 0)
        count2 = available_overlaps.get((layer2, layer1), 0)
        if count1 == 0 and count2 == 0:
            # no overlaps for those two layers
            raise ValueError(f"No overlaps found for the pair of layers: ('{layer1}', '{layer2}').\nAvailable overlaps: {available_overlaps}\nTo precompute the overlaps between this two layers (and store them in mongodb), execute:\n    python geo-loader.py -C config.ini --areas-overlap {layer1} {layer2}\nFor more info: https://gitlab.bsc.es/flowmaps/mov_loader/-/blob/master/README.md")
        elif count1 > count2:
            pipeline = [
                {
                    '$match': {
                        'l.layer': layer1, 
                        'm.layer': layer2,
                        'l.ratio': {
                            '$gte': layer1_min_ratio
                        },
                        'm.ratio': {
                            '$gte': layer2_min_ratio
                        }
                    }
                }, {
                    '$project': {
                        'id1': '$l.id', 
                        'id2': '$m.id', 
                        'ratio1': '$l.ratio', 
                        'ratio2': '$m.ratio'
                    }
                }
            ]
        else:
            pipeline = [ # reversed...
                {
                    '$match': {
                        'l.layer': layer2,
                        'm.layer': layer1,
                        'l.ratio': {
                            '$gte': layer2_min_ratio
                        },
                        'm.ratio': {
                            '$gte': layer1_min_ratio
                        }
                    }
                }, {
                    '$project': {
                        'id2': '$l.id', 
                        'id1': '$m.id', 
                        'ratio2': '$l.ratio', 
                        'ratio1': '$m.ratio'
                    }
                }
            ]
        self.add_steps(pipeline)

    def get(self):
        cursor = self._execute_query()
        return OverlapsCursor(cursor, self.layer1, self.layer2, self.db)


class OverlapsCursor(Cursor):

    def __init__(self, cursor, layer1, layer2, db):
        self.layer1 = layer1
        self.layer2 = layer2
        self.db = db
        super().__init__(cursor)

    def to_dict(self):
        """
        Returns: dict of list(tuple)
            
            Example: 

            .. code-block:: text

                overlaps_dict = {'source_id': [('target_id', ratio), ...], ...}
                
                overlaps_dict['0810101']  # [('08', 1.0)] this means that the 100% of the area '0810101’ is contained in Barcelona

        """
        overlaps = defaultdict(list)
        for doc in self._cursor:
            overlaps[doc['id1']].append((doc['id2'], doc['ratio1']))
        for k in overlaps:
            overlaps[k] = sorted(overlaps[k], key=lambda x: -x[1])
        return overlaps

    def to_csr_matrix(self, row_map=None, col_map=None, grow_rows=False, grow_cols=False):
        """
        Returns a scipy CSR matrix containing the overlaps between both layers.
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.csr_matrix.html#scipy.sparse.csr_matrix

        The rows represent the layer1 areas, and the columns represent the layer2 areas.

        Returns: 3-tuple

            matrix: csr matrix containing the overlaps
            
            row_map (dict): dict containing the map id-index for layer1 (rows)
            
            col_map (dict): dict containing the map id-index for layer2 (columns)
        """

        (row_map, grow_rows) = (dict(row_map), grow_rows) if row_map else ({}, True)
        (col_map, grow_cols) = (dict(col_map), grow_cols) if col_map else ({}, True)
        data = []
        col = []
        row = []
        for doc in self._cursor:
            if grow_rows or doc['id1'] in row_map:
                idx1 = row_map.setdefault(doc['id1'], len(row_map))
            else:
                continue
            if grow_cols or doc['id2'] in col_map:
                idx2 = col_map.setdefault(doc['id2'], len(col_map))
            else:
                continue
            data.append(doc['ratio1'])
            row.append(idx1)
            col.append(idx2)
        return csr_matrix((data, (row, col))), row_map, col_map

    def to_matrix(self, row_ids=None, col_ids=None):
        """
        Returns a scipy CSR matrix containing the overlaps between both layers.
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.csr_matrix.html#scipy.sparse.csr_matrix

        The rows represent the layer1 areas, and the columns represent the layer2 areas.

        Returns: matrix

            matrix: csr matrix containing the overlaps
            
        """
        if row_ids is None:
            row_ids = LayerQuery(self.db, self.layer1).ids()
        if col_ids is None:
            col_ids = LayerQuery(self.db, self.layer2).ids()
        data = []
        col = []
        row = []
        for doc in self._cursor:
            i = row_ids.index(doc['id1'])
            j = col_ids.index(doc['id2'])
            data.append(doc['ratio1'])
            row.append(i)
            col.append(j)
        return csr_matrix((data, (row, col)), shape=(len(row_ids), len(col_ids)))
