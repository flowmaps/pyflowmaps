#!/usr/bin/env python
# -*- coding: utf-8 -*-

import setuptools
import re
import os
import sys

# In this way, we are sure we are getting
# the installer's version of the library
# not the system's one
setupDir = os.path.dirname(__file__)
sys.path.insert(0,setupDir)

from flowmaps import version as pyflowmaps_version
import flowmaps

# Populating the long description
with open(os.path.join(setupDir,"README.md"), "r", encoding="utf-8") as fh:
	long_description = fh.read()

# Populating the install requirements
with open(os.path.join(setupDir,'requirements.txt'), encoding="utf-8") as f:
	requirements = []
	egg = re.compile(r"#[^#]*egg=([^=&]+)")
	for line in f.read().splitlines():
		m = egg.search(line)
		requirements.append(line  if m is None  else m.group(1))

# This block will be useful if some static file must be shipped along with the library
data_files = []
# Gathering the list of generated files
#for reldirname,ext in [("mov_loader/generated",'.json')]:
#	absdirname = os.path.join(setupDir,reldirname)
#	relfilelist = []
#	for entry in os.scandir(absdirname):
#		# We are avoiding to enter in loops around '.' and '..'
#		if entry.name[0] != '.' and entry.is_file(follow_symlinks=False) and entry.name.endswith(ext):
#			relfilelist.append(os.path.join(reldirname,entry.name))
#	
#	if len(relfilelist) > 0:
#		data_files.append((reldirname,relfilelist))


setuptools.setup(
	name=flowmaps.name,
	version=pyflowmaps_version,
	scripts=[],
	data_files=data_files,
	author="Javier del Valle",
	author_email="javier.delvalle@bsc.es",
	description="Flow-Maps python library",
	long_description=long_description,
	long_description_content_type="text/markdown",
	url="https://gitlab.bsc.es/flowmaps/pyflowmaps.git",
	packages=setuptools.find_packages(),
	install_requires=requirements,
        license='Apache',
	classifiers=[
		"Programming Language :: Python :: 3",
		"License :: OSI Approved :: Apache Software License",
		"Operating System :: OS Independent",
	],
)
