
# Pyflowmaps

Python package to access FlowMaps database.


## Installation

Install python dependencies:

	pip install -r requirements.txt

If you want to build the documentation you also have to install next dependencies

	pip install -r requirements-documentation.txt

Run tests. You have to copy [test.json.template](test.json.template) to something like `test.json`, and set up the credentials:

	pytest-3 test.py test.json


## Quickstart: plot Covid-19 cases by province

```python
import pytz
from datetime import datetime
from flowmaps.flowmaps import FlowMaps

tz = pytz.timezone('Europe/Madrid')

mongodb_params = {
    "host": ['mongomachine:27017'],
    "username":'a_mongo_user',
    "password": '****',
    "authSource": 'FlowMaps'
}

db = FlowMaps(mongodb_params)

start_date = tz.localize(datetime(2020, 7, 1, 0, 0, 0))
end_date = tz.localize(datetime(2020, 7, 30, 0, 0, 0))

# build a query to retrieve data from ES.covid_cpro dataset (Covid-19 cases by province)
query = db.data(start_date, end_date, ev='ES.covid_cpro')

# convert the nested d.num_casos field to a root level 'cases' field
query = query.flatten_fields({'d.num_casos': 'cases'})

# group by id, and count the cases (sum all the cases for each province in the date period)
query = query.group_by(['id'], accum_fields=['cases'])

# print the query that will be executed
# print(query)

# get the data into a dataframe
cases_df = query.get().to_df()

# plot provinces. The color represents the number of reported cases in the period
provinces_geojson.plot(df=cases_df, loc='id', color_field='cases', colorscale='Reds')
```

More examples in **notebooks/Examples.ipynb**
