import pytz
from datetime import datetime, timezone, timedelta

from flowmaps.flowmaps import FlowMaps
from flowmaps.cursor import Cursor
from flowmaps.mobility import MobilityCursor

import sys
import json

# MongoDB params have to be initialized
mongodb_params = { }

# mongodb_params = {
#     "host": ['localhost:27017'],
#     # "username":'a_mongo_user',
#     # "password": 'its_password',
#     # "authSource": 'FlowMaps'
# }


tz = pytz.timezone('Europe/Madrid')



def test_layer():

    fm = FlowMaps(mongodb_params)
    hospitales = fm.layer('hospitales').get()
    hospi = next(hospitales)
    assert 'id' in hospi
    assert 'feat' in hospi

    hospitales = fm.layer('hospitales').get().simplify_polygons()
    hospi = next(hospitales)
    assert 'id' in hospi
    assert 'feat' in hospi

    hospitales = fm.layer('hospitales').get().simplify_polygons().to_geojson()
    assert 'features' in hospitales 
    assert hospitales['type'] == 'FeatureCollection'

    hospital = fm.layer('hospitales', id='020030').get_first()
    assert hospital['id'] == '020030'
    assert 'feat' in hospi

    provincia = fm.layer('cnig_provincias', id='37').get_first().to_geojson().simplify_polygons(ratio=0.001, min_points=10)
    assert 'type' in provincia
    assert provincia['type'] == 'FeatureCollection'

    ids = ['37', '38']
    provincias = fm.layer('cnig_provincias').filter({'id': {'$in': ids}}).get()
    provincia = next(provincias)
    assert provincia['id'] in ids

    hospitalet = fm.layer('cnig_municipios', id='08101').get_first()
    geometry = hospitalet['feat']['geometry']
    areas = hospitalet.query_overlapping_polygons('abs_09', use_centroid=True).get()
    area = next(areas)
    assert area['layer'] == 'abs_09'
    assert 'id' in area
    assert 'feat' in area

    provincias = fm.layer('cnig_provincias').get().to_index(key='id')
    assert provincias['38']['feat']['properties']['rotulo'] == 'Santa Cruz de Tenerife'


def test_overlaps():
    fm = FlowMaps(mongodb_params)
    mitma_to_mun = fm.overlaps('mitma_mov', 'cnig_municipios', layer1_min_ratio=0.01).get().to_dict()
    assert mitma_to_mun['0810103'][0][0] == '08101'


def test_data():
    fm = FlowMaps(mongodb_params)

    start_date = tz.localize(datetime(2020, 6, 1, 0, 0, 0))
    end_date = tz.localize(datetime(2020, 6, 2, 0, 0, 0))
    history = fm.data(start_date, end_date, ev='09.covid_abs', layer='abs_09', id='282').get()
    history = history.to_list()
    assert len(history) > 0
    assert all(point['evstart'] >= start_date for point in history)
    assert all(point['evstart'] <= end_date for point in history)

    start_date = tz.localize(datetime(2020, 6, 1, 0, 0, 0))
    end_date = tz.localize(datetime(2020, 6, 2, 0, 0, 0))
    df = fm.data(start_date, end_date, ev='09.covid_abs').flatten_fields({'d.cases': 'cases', 'd.suspects': 'suspects'}).group_by(['id'], ['cases', 'suspects']).get().to_df()
    assert 'cases' in df
    assert 'suspects' in df
    assert 'id' in df

    # aggregation locally vs aggregation in mongo
    start_date = tz.localize(datetime(2020, 6, 1, 0, 0, 0))
    end_date = tz.localize(datetime(2020, 6, 10, 0, 0, 0))
    cases_by_day = fm.data(start_date, end_date, layer_id='abs_09').flatten_fields({'d.cases': 'cases'}).group_by(['id', 'date'], ['cases']).get()
    cases_by_day2 = fm.data(start_date, end_date, layer_id='abs_09').flatten_fields({'d.cases': 'cases'}).get().group_by(['id', 'date'], ['cases'])
    assert cases_by_day.aggregate(key='id', value='cases')['106'] == cases_by_day2.aggregate(key='id', value='cases')['106']



def test_mobility():
    fm = FlowMaps(mongodb_params)

    start_date = tz.localize(datetime(2020, 6, 1, 0, 0, 0))
    end_date = tz.localize(datetime(2020, 6, 2, 0, 0, 0))
    movs = fm.mobility('ine_mov.movements', start_date, end_date).filter({'destino': '001A'}).get()
    mov = next(movs)
    assert 'flujo' in mov
    assert mov['flujo'] > 0
    assert 'origen' in mov
    assert 'destino' in mov

    start_date = tz.localize(datetime(2020, 6, 1, 0, 0, 0))
    end_date = tz.localize(datetime(2020, 6, 2, 0, 0, 0))
    movs = fm.mobility('ine_mov.movements', start_date, end_date).filter({'origen': '001A'}).group_by(['origen', 'destino'], ['flujo']).get()
    mov = next(movs)
    assert 'flujo' in mov
    assert mov['flujo'] > 0
    assert 'origen' in mov
    assert 'destino' in mov

    start_date = tz.localize(datetime(2020, 6, 1, 0, 0, 0))
    end_date = tz.localize(datetime(2020, 6, 2, 0, 0, 0))
    movs = fm.mobility('mitma_mov.movements_raw', start_date, end_date).filter({'destino': '38033_AM'}).group_by(['origen', 'destino'], ['viajes', 'viajes_km']).get()
    mov = next(movs)
    assert 'viajes' in mov
    assert mov['viajes'] > 0
    assert mov['viajes_km'] > 0
    assert 'origen' in mov
    assert 'destino' in mov

    start_date = tz.localize(datetime(2020, 6, 1, 0, 0, 0))
    end_date = tz.localize(datetime(2020, 6, 2, 0, 0, 0))
    ids = ['0810101', '0810102', '0810103', '0810104', '0810105', '0810106']
    movs = fm.mobility('mitma_mov.movements_raw', start_date, end_date).filter({'destino': {'$in': ids}, 'origen': {'$in': ids}}).group_by(['origen', 'destino'], ['viajes', 'viajes_km']).get()
    mov = next(movs)
    assert mov['origen'] in ids
    assert mov['destino'] in ids

    start_date = tz.localize(datetime(2020, 6, 1, 0, 0, 0))
    end_date = tz.localize(datetime(2020, 6, 2, 0, 0, 0))
    ids = ['0810101', '0810102', '0810103', '0810104', '0810105', '0810106']
    movements = fm.mobility('mitma_mov.movements_raw', start_date, end_date).filter({'destino': {'$in': ids}, 'origen': {'$in': ids}}).get().to_dictdict()
    assert movements['0810101']['0810102'] > 0

    start_date = tz.localize(datetime(2020, 6, 1, 0, 0, 0))
    end_date = tz.localize(datetime(2020, 6, 2, 0, 0, 0))
    ids = ['0810101', '0810102', '0810103', '0810104', '0810105', '0810106']
    movements = fm.mobility('mitma_mov.movements_raw', start_date, end_date).filter({'destino': {'$in': ids}, 'origen': {'$in': ids}}).get().to_dictdict()
    assert movements['0810101']['0810102'] > 0

    # aggregation locally vs aggregation in mongo
    start_date = tz.localize(datetime(2020, 6, 1, 0, 0, 0))
    end_date = tz.localize(datetime(2020, 6, 2, 0, 0, 0))
    movs_by_day1 = fm.mobility('mitma_mov.movements_raw', start_date, end_date).filter({'origen': '0810101'}).get().group_by(['origen', 'destino'], ['viajes', 'viajes_km']).to_df()
    movs_by_day2 = fm.mobility('mitma_mov.movements_raw', start_date, end_date).filter({'origen': '0810101'}).group_by(['origen', 'destino'], ['viajes', 'viajes_km']).get().to_df()
    assert movs_by_day1[movs_by_day1['destino'] == '08001']['viajes'].iloc[0] - movs_by_day2[movs_by_day2['destino'] == '08001']['viajes'].iloc[0] < 0.00001


def test_interpolate():

    docs = [{'id': 'aaaa', 'cases': 10},
            {'id': 'bbbb', 'cases': 50}]

    overlaps = {
        'aaaa': [('A', 0.7), ('B', 0.3)],
        'bbbb': [('A', 0.2), ('B', 0.1), ('C', 0.6), ('D', 0.1)],
    }

    cursor = Cursor(docs)
    cursor2 = cursor.interpolate(overlaps, interpolate_fields=['cases'], show_warnings=True)
    cursor2 = cursor2.group_by(['id'], ['cases'])
    cases_by_id = cursor2.to_index(key='id', value='cases')
    assert cases_by_id['A'] == 10*0.7 + 50*0.2
    assert cases_by_id['B'] == 10*0.3 + 50*0.1
    assert cases_by_id['C'] == 50*0.6
    assert cases_by_id['D'] == 50*0.1


    docs = [{'origen': 'aaaa', 'destino': 'bbbb', 'myviajes': 10},
            {'origen': 'bbbb', 'destino': 'cccc', 'myviajes': 30}]

    overlaps = {
        'aaaa': [('A', 0.7), ('B', 0.3)],
        'bbbb': [('A', 0.2), ('B', 0.1), ('C', 0.6), ('D', 0.1)],
        'cccc': [('D', 0.2), ('E', 0.1), ('F', 0.6)],
    }

    from unittest.mock import MagicMock
    fm = MagicMock()
    fm.overlaps().get().to_dict = lambda: overlaps
    cursor = MobilityCursor(docs, 'test.collection')
    cursor2 = cursor.interpolate_origin_layer(fm, 
                                source_layer='layer1',
                                target_layer='layer2',
                                fields=['myviajes'],
                                show_warnings=True)
    cursor2 = cursor2.group_by(['origen', 'destino'], ['myviajes'])
    viajes = cursor2.to_dictdict(aggregate_field='myviajes')
    # print(dict(viajes))
    assert viajes['A']['bbbb'] == 7 
    assert viajes['A']['cccc'] == 6
    assert viajes['B']['bbbb'] == 3
    assert viajes['D']['cccc'] == 3

    fm = MagicMock()
    fm.overlaps().get().to_dict = lambda: overlaps
    cursor = MobilityCursor(docs, 'test.collection')
    cursor2 = cursor.interpolate_destination_layer(fm, 
                                source_layer='layer1',
                                target_layer='layer2',
                                fields=['myviajes'],
                                show_warnings=True)
    cursor2 = cursor2.group_by(['origen', 'destino'], ['myviajes'])
    viajes = cursor2.to_dictdict(aggregate_field='myviajes')
    # print(dict(viajes))
    assert viajes['aaaa']['A'] == 2 
    assert viajes['aaaa']['B'] == 1 
    assert viajes['aaaa']['C'] == 6 
    assert viajes['bbbb']['D'] == 6 


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Usage: {} {{json config}}".format(sys.argv[0]))
        sys.exit(1)
    
    with open(sys.argv[1],mode="r",encoding="utf-8") as fh:
        mongodb_params = json.load(fh)
    # test_layer()
    # test_overlaps()
    # test_data()
    # test_mobility()
    test_interpolate()
