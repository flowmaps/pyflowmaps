.. Pyflowmaps documentation master file, created by
   sphinx-quickstart on Mon Oct  5 14:29:27 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Pyflowmaps's documentation!
======================================



Installation
************

Install python dependencies:

.. code-block:: bash

	pip install -r requirements.txt

Run tests (the tests need access to the Mongodb server, otherwise they will fail):

.. code-block:: bash

	pytest-3 test.py


Quickstart example: Plot Covid-19 cases by province 
***************************************************

More examples can be found in *notebooks/Examples.ipynb*

.. code-block:: python

	import pytz
	from datetime import datetime
	from flowmaps.flowmaps import FlowMaps

	tz = pytz.timezone('Europe/Madrid')

	start_date = tz.localize(datetime(2020, 7, 1, 0, 0, 0))
	end_date = tz.localize(datetime(2020, 7, 30, 0, 0, 0))

	mongodb_params = {
	    "host": ['mongomachine:27017'],
	    "username":'a_mongo_user',
	    "password": '****',
	    "authSource": 'FlowMaps'
	}

	db = FlowMaps(mongodb_params)

	# build a query to retrieve data from ES.covid_cpro dataset (Covid-19 cases by province)
	query = db.data(start_date, end_date, ev='ES.covid_cpro')

	# convert the nested d.num_casos field to a root level 'cases' field
	query = query.flatten_fields({'d.num_casos': 'cases'})

	# group by id, and count the cases (sum all the cases for each province in the date period)
	query = query.group_by(['id'], accum_fields=['cases'])

	# print the query that will be executed
	# print(query)

	# get the data into a pandas dataframe
	cases_df = query.get().to_df()

	# get the provinces layer and convert it to geojson
	provinces_geojson = db.layer('cnig_provincias').get().to_geojson()

	# plot provinces. The color represents the number of reported cases in the period
	provinces_geojson.plot(df=cases_df, loc='id', color_field='cases', colorscale='Reds')


.. image:: images/example1.png
   :width: 800



.. toctree::
   :maxdepth: 3
   :caption: Contents:

   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
