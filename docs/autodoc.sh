#!/bin/bash

export PYTHONPATH=../:$PYTHONPATH

sphinx-apidoc -f -o source/ ../flowmaps

make html

sensible-browser --new-tab build/html/index.html


